package uk.ac.uos;

//This is a class to contain possible symbols and syntax
//in JSOn
public class Symbol {
	
	//This enum contains the possible values and syntax
	public enum Type {LEFT_SQUARE_BRACKET, LEFT_CURLY_BRACKET, RIGHT_SQUARE_BRACKET,
		RIGHT_CURLY_BRACKET, COLON, COMMA, TRUE_BOOLEAN_VALUE, FALSE_BOOLEAN_VALUE,
		NULL_BOOLEAN_VALUE, STRING_VALUE, NUMBER_VALUE, SPACE};
	
	//This is the enum type
	public final Type type;
	//And this is the possible value of that type so it can store
	//text such as the text of a string
	public final String value;
		
	/**
	 * Constructor for a symbol with both a type and a value
	 * @param type
	 * @param value
	 */
	public Symbol(Type type, String value) {
		this.type = type;
		this.value = value;
	}
	
	/**
	 * Constructor for a symbol which does not have a value
	 * @param type
	 */
	public Symbol(Type type) {
		this(type, null);
	}
}
