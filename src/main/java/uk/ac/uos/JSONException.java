package uk.ac.uos;

//'Serial' allows JSONException to be
//added to a serial object stream. I do not need to implement
//this so I am suppressing the warning.
@SuppressWarnings("serial")
public class JSONException extends Exception{

	/**
	 * This constructor allows this exception to take a custom
	 * message. I'm using the standard Exception implementation
	 * of this by using the super constructor and passing the
	 * message to this.
	 * @param message
	 */
	public JSONException(String message) {
		super(message);
	}

}
