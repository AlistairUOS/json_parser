package uk.ac.uos;

import java.io.IOException;
import java.io.PushbackReader;
import java.io.StringReader;

//This is the class to analyse the syntax of JSON and determine
//the symbol type
public class LexicalAnalyser {
	
	//Here I am using a pushback reader because this allowed me
	//to move back in the JSON rather than always having to work
	//forward. As a result I could check ahead in code and then 
	//go back so no characters were missed.
	private PushbackReader pushbackReader;
	
	
	/**
	 * This is the constructor which requires a reader to be 
	 * passed in. I did this as it is faster way to set the reader
	 * that I want to step through rather than getter and setter 
	 * methods.
	 * @param reader
	 */
	public LexicalAnalyser(StringReader reader) {
		pushbackReader = new PushbackReader(reader);
	}
	
	
	/**
	 * I have chosen to use one method to step through the
	 * reader. I could have used multiple methods but I used
	 * one for the ease of testing.
	 * @return Symbol
	 * @throws IOException
	 * @throws JSONException
	 */
	public Symbol next() throws IOException, JSONException {
		Symbol result = null;
		//This moves on to the next character of the pushback 
		//reader.
		int character = pushbackReader.read();
		
		//This condition statement checks to see if this character
		//is the last character. This will then result in the
		//method returning null so that I know that the parser has
		//finished.
		if ( -1 != character ) {
			//The following if statements will check single 
			//characters which is fairly simple.
			if ( character == '[' ) {
				result = new Symbol(Symbol.Type.LEFT_SQUARE_BRACKET);
			}
			else if ( character == '{' ) {
				result = new Symbol(Symbol.Type.LEFT_CURLY_BRACKET);
			}
			else if ( character == ']' ) {
				result = new Symbol(Symbol.Type.RIGHT_SQUARE_BRACKET);
			}
			else if ( character == '}' ) {
				result = new Symbol(Symbol.Type.RIGHT_CURLY_BRACKET);
			}
			else if ( character == ':' ) {
				result = new Symbol(Symbol.Type.COLON);
			}
			else if ( character == ',' ) {
				result = new Symbol(Symbol.Type.COMMA);
			}
			//This is checking for whitespace. I am using the
			//.isWhitespace method because it can check for
			//both spaces, tabs and new lines. Otherwise I would 
			//have to have separate if conditions for each.
			else if (Character.isWhitespace(character)) {
				while (Character.isWhitespace(character)) {
					character = pushbackReader.read();
				}
				result = new Symbol(Symbol.Type.SPACE);
				//Here is the use of unread. Because I had to check
				//the next character to know if the whitespace was 
				//finished I have to move back a character so I don't
				//miss one.
				pushbackReader.unread(character);
			}
			//Here I am checking for a string so I have to build up
			//the string based on the characters I find and then 
			//create a Symbol Type with a value that contains the value
			//of the string.
			else if ( character == '"' ) {
				String stringBuilder = "";
				//Will loop through the string until it finds a string
				character = pushbackReader.read();
				while (character != '"') {
					stringBuilder += (char) character;
					character = pushbackReader.read();
				}
				result = new Symbol(Symbol.Type.STRING_VALUE, stringBuilder);
			}
			//This next if is for any of the other types 
			//which are booleans or numbers.
			//Here I am checking for a true value. In 
			//order to be valid the whole word true
			//must be present.
			else if ( character == 't' ) {
				character = pushbackReader.read();
				if ( character == 'r' ) {
					character = pushbackReader.read();
					if ( character == 'u' ) {
						character = pushbackReader.read();
						if ( character == 'e' ) {
							result = new Symbol(Symbol.Type.TRUE_BOOLEAN_VALUE);
						} else {
							throw new JSONException("BOOM! Error. Invalid true value.");
						}
					} else {
						throw new JSONException("BOOM! Error. Invalid true value.");
					}
				} else {
					throw new JSONException("BOOM! Error. Invalid true value.");
				}		
			}
			else if ( character == 'n' ) {
				character = pushbackReader.read();
				if ( character == 'u' ) {
					character = pushbackReader.read();
					if ( character == 'l' ) {
						character = pushbackReader.read();
						if ( character == 'l' ) {
							result = new Symbol(Symbol.Type.NULL_BOOLEAN_VALUE);
						} else {
							throw new JSONException("BOOM! Error. Invalid null value.");
						}
					} else {
						throw new JSONException("BOOM! Error. Invalid null value.");
					}
				} else {
					throw new JSONException("BOOM! Error. Invalid null value.");
				}		
			}
			else if ( character == 'f' ) {
				character = pushbackReader.read();
				if ( character == 'a' ) {
					character = pushbackReader.read();
					if ( character == 'l' ) {
						character = pushbackReader.read();
						if ( character == 's' ) {
							character = pushbackReader.read();
							if ( character == 'e' ) {
								result = new Symbol(Symbol.Type.FALSE_BOOLEAN_VALUE);
							} else {
								throw new JSONException("BOOM! Error. Invalid false value.");
							}
						} else {
							throw new JSONException("BOOM! Error. Invalid false value.");
						}
					} else {
						throw new JSONException("BOOM! Error. Invalid false value.");
					}
				} else {
					throw new JSONException("BOOM! Error. Invalid false value.");
				}		
			}
			//A number starts with either a number or the '-' character because it
			//could be a negative number.
			else if (Character.isDigit(character) || character == '-') {
				String numberBuilder = "";
				//Will loop through the reader until there is a character that is not
				//possible to have in a number. Digits, decimal points, plus and minus
				//characters and e or E characters are all valid.
				while (Character.isDigit(character) || character == '.' || character == '-' || character == '+' || character == 'e' || character == 'E') {
					numberBuilder += (char) character;
					//Need to push back because the loop is reading the character ahead. If the character ahead is
					//not a valid number character than the reader must go backwards or that character
					//will be missed by the rest of the parser.
					character = pushbackReader.read();
				}
				result = new Symbol(Symbol.Type.NUMBER_VALUE, numberBuilder);
				pushbackReader.unread(character);
			}
			
			
			
		}
		
		return result;
	}
	
	

}
