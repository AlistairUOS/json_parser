package uk.ac.uos;

import java.util.ArrayList;
import java.util.List;

public class JSONArray {
	
	List<Object> arrayContents = new ArrayList<Object>();
	
	/**
	 * Putting an array list argument in the constructor so that an JSON
	 * array can be parsed in in the constructor.
	 * @param array ArrayList<Object>
	 */
	JSONArray(ArrayList<Object> array){
		this.arrayContents = array;
	}

}
