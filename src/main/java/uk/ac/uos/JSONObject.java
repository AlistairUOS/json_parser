package uk.ac.uos;

import java.util.HashMap;
import java.util.Map;

public class JSONObject {
	
	Map<String, Object> objectContents;
	
	/**
	 * Putting an array list argument in the constructor so that an JSON
	 * object can be parsed in in the constructor.
	 * @param map
	 */
	JSONObject(HashMap<String, Object> map){
		this.objectContents = map;
	}

}
