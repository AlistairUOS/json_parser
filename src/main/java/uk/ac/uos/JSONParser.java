package uk.ac.uos;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;

public class JSONParser {
	
	//Creating a version of the lexical analyser so I can use the code from that class here.
	LexicalAnalyser lexicalAnalyser;
	//This is the main JSON object. All JSON must have an initial object which contains other values.
	//I've made it public and put it here so it can be accessed by my tests.
	public JSONObject mainObject;
	
	
	/**
	 * This is the main parsing method that my tests will call.
	 * @param JSONText
	 * @throws IOException
	 * @throws JSONException
	 */
	public void parse(String JSONText) throws IOException, JSONException {
		//Converting the JSON String into a reader so it can be lexically analysed.
		lexicalAnalyser = new LexicalAnalyser(new StringReader(JSONText));
		
		//Gathering the first symbol from the reader.
		Symbol currentSymbol = lexicalAnalyser.next();
		
		//Iterating through the main body of the JSON. And turning text into my own representative symbols.
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		while ( currentSymbol != null ) {
			listOfSymbols.add(currentSymbol);
			currentSymbol = lexicalAnalyser.next();
		}
		
		//Removing all the whitespace from the JSON.
		listOfSymbols = removeWhiteSpace(listOfSymbols);
		
		//Parsing the JSON which is an object to begin with.
		mainObject = parseObject(listOfSymbols);
	}
	
	
	/**
	 * This is the method for parsing a Symbol Type that I believe to be a String.
	 * @param input
	 * @return String
	 * @throws JSONException
	 */
	public String parseString(Symbol input) throws JSONException {
		String result = null;
		if ( input.type == Symbol.Type.STRING_VALUE ) {
			result = input.value;
		} else {
			throw new JSONException("BOOM! Error. This is not a string.");
		}
		return result;
	}
	
	/**
	 * This is the method for parsing a symbol type that I believe to be a boolean.
	 * @param input+
	 * @return boolean
	 * @throws JSONException
	 */
	public boolean parseBoolean(Symbol input) throws JSONException {
		if (input.type == Symbol.Type.TRUE_BOOLEAN_VALUE) {
			return true;
		} else if ( input.type == Symbol.Type.FALSE_BOOLEAN_VALUE ) {
			return false;
		} else {
			throw new JSONException("BOOM! Error. This is not a boolean value.");
		}
	}
	
	/**
	 * This is the method for parsing a symbol type that I believe to be null.
	 * @param input
	 * @return Object
	 * @throws JSONException
	 */
	public Object parseNull(Symbol input) throws JSONException {
		if (input.type != Symbol.Type.NULL_BOOLEAN_VALUE) {
			throw new JSONException("BOOM! Error. This is not null.");
		}
		return null;
	}
	

	/**
	 * 	This is the method for parsing a symbol type that I believe is an integer.
	 *  I am using a Number class return type because this allows it to handle both 
	 *  integers and floating point numbers.
	 * @param input
	 * @return Number
	 * @throws JSONException
	 */
	public Number parseNumber(Symbol input) throws JSONException {
		if ( input.type == Symbol.Type.NUMBER_VALUE ) {
			if ( !input.value.contains(".")) {
				return Long.parseLong(input.value);
			} else {
				return Double.parseDouble(input.value);
			}
		} else {
			throw new JSONException("BOOM! Error. This is not a number.");
		}
	}
	
	
	/**
	 * To parse an array I must pass in a series of symbols in the
	 * form of a list because an array is made up of multiple values.
	 * @param input
	 * @return JSONArray
	 * @throws JSONException
	 */
	public JSONArray parseArray(ArrayList<Symbol> input) throws JSONException {
		ArrayList<Object> listOfJSONValues = new ArrayList<Object>();
		
		//First I will check it is a valid array by seeing if
		//the first and the last symbols are square brackets because
		//all arrays in JSON start and end with square brackets.
		//Therefore it also cannot be smaller than 2 symbols.
		//Note that at this point all white space has been removed.
		if ( input.size() < 2 || input.get(0).type != Symbol.Type.LEFT_SQUARE_BRACKET || input.get(input.size() - 1).type != Symbol.Type.RIGHT_SQUARE_BRACKET) {
			throw new JSONException("BOOM! Error. This is not an array.");
		}
		
		//Now I will iterate through each of the symbols in the array
		//(apart from the opening and closing brackets) and parse each
		//one. I will also ensure that there are commas seperating the 
		//JSON values.
		//Starting at 1 to miss the opening bracket. 
		//Ending 1 before the end to miss the last bracket.
		//Skipping by two to jump over the commas.
		for ( int i = 1; i < input.size() - 1; i = i + 2) {
			Symbol currentSymbol = input.get(i);
			switch(currentSymbol.type) {
				case TRUE_BOOLEAN_VALUE :
					listOfJSONValues.add(parseBoolean(currentSymbol));
					break;
				case FALSE_BOOLEAN_VALUE :
					listOfJSONValues.add(parseBoolean(currentSymbol));
					break;
				case NULL_BOOLEAN_VALUE :
					listOfJSONValues.add(parseNull(currentSymbol));
					break;
				case STRING_VALUE :
					listOfJSONValues.add(parseString(currentSymbol));
					break;
				case NUMBER_VALUE :
					listOfJSONValues.add(parseNumber(currentSymbol));
					break;
				//This is a special case. An array is made up of more than one symbol type obviously.
				//Therefore I will have to capture the full size of the array and pass it back to this method
				//hence the recursive nature.
				case LEFT_SQUARE_BRACKET :
					ArrayList<Symbol> newArrayOfSymbols = new ArrayList<Symbol>();
					//Storing the array contents up to the end of the array and then progressing the array reading
					//at this level.
					while ( currentSymbol.type != Symbol.Type.RIGHT_SQUARE_BRACKET ) {
						newArrayOfSymbols.add(currentSymbol);
						i++;
						currentSymbol = input.get(i);
					}
					//The line below will add the right square bracket
					newArrayOfSymbols.add(currentSymbol);
					listOfJSONValues.add(parseArray(newArrayOfSymbols));
					break;
				case LEFT_CURLY_BRACKET :
					ArrayList<Symbol> newArrayOfSymbolsForObject = new ArrayList<Symbol>();
					//Storing the array contents up to the end of the array and then progressing the array reading
					//at this level.
					while ( currentSymbol.type != Symbol.Type.RIGHT_CURLY_BRACKET ) {
						newArrayOfSymbolsForObject.add(currentSymbol);
						i++;
						currentSymbol = input.get(i);
					}
					//The line below will add the right square bracket
					newArrayOfSymbolsForObject.add(currentSymbol);
					listOfJSONValues.add(parseObject(newArrayOfSymbolsForObject));
					break;
				default :
					throw new JSONException("BOOM! Error. An array cannot contain this.");
			}
			//I am checking that the next character is a comma as long as this is not the last
			//character of the array.
			if ( input.get(i + 1).type != Symbol.Type.COMMA && i != input.size() - 2) {
				throw new JSONException("BOOM! Error. Missing comma in array.");
			}
		}	
		JSONArray jsonArray = new JSONArray(listOfJSONValues);
		return jsonArray;
	}
	

	/**
	 * 	Parsing an object will be similar to parsing a string. 
	 *  First I need to check that it starts and ends with curly brackets.
	 * @param input
	 * @return JSONObject
	 * @throws JSONException
	 */
	public JSONObject parseObject(ArrayList<Symbol> input) throws JSONException {
		HashMap<String, Object> objectContents = new HashMap<String, Object>();
		
		if ( input.size() < 2 || input.get(0).type != Symbol.Type.LEFT_CURLY_BRACKET || input.get(input.size() - 1).type != Symbol.Type.RIGHT_CURLY_BRACKET) {
			throw new JSONException("BOOM! Error. This is not an object.");
		}
		
		//This if will stop empty objects getting in.
		if ( input.size() > 4) {
			//Starting at 1 to skip over the first curly bracket. And ending early 1 to miss the last.
			//Incrementing 3 each time because basic JSON key and value pairs are three symbols
			//a string, a colon and a value.
			for ( int i = 1; i < input.size() - 3; i = i + 4) {
				Symbol currentKey = input.get(i);
				Symbol colon = input.get(i + 1);
				Symbol currentValue = input.get(i + 2);
				ArrayList<Symbol> keyValuePair = new ArrayList<Symbol>();
				keyValuePair.add(currentKey);
				keyValuePair.add(colon);
				
				//Now I must make an exception for arrays and objects so it will keep adding until it ends.
				if ( currentValue.type == Symbol.Type.LEFT_SQUARE_BRACKET ) {
					while ( currentValue.type != Symbol.Type.RIGHT_SQUARE_BRACKET ) {
						keyValuePair.add(currentValue);
						i++;
						currentValue = input.get(i + 2);
					}
					keyValuePair.add(currentValue);
					//To parse an object I must pass it back into this method. Similar to how I parsed arrays.
				} else if ( currentValue.type == Symbol.Type.LEFT_CURLY_BRACKET ) {
					while ( currentValue.type != Symbol.Type.RIGHT_CURLY_BRACKET ) {
						keyValuePair.add(currentValue);
						i++;
						currentValue = input.get(i + 2);
					}
					keyValuePair.add(currentValue);
				} else {
					keyValuePair.add(currentValue); 
				}
				
				ArrayList<Object> keyValuePairParsed = parseKeyValuePair(keyValuePair);
				objectContents.put((String) keyValuePairParsed.get(0), keyValuePairParsed.get(1));
				
				if ( input.get(i + 3).type != Symbol.Type.COMMA && i != input.size() - 4) {
					throw new JSONException("BOOM! Error. Missing comma in object.");
				}
			}
		}
		
		
		JSONObject jsonObject = new JSONObject(objectContents);
		return jsonObject;
	}
	
	/**
	 * This method is calculating a key a value pair from a list of characters.
	 * @param input
	 * @return ArrayList<Object>
	 * @throws JSONException
	 */
	public ArrayList<Object> parseKeyValuePair(ArrayList<Symbol> input) throws JSONException{
		ArrayList<Object> pair = new ArrayList<Object>();
		
		//Here I will validate that this is actually a key value pair.
		if ( input.size() < 3 || input.get(0).type != Symbol.Type.STRING_VALUE || input.get(1).type != Symbol.Type.COLON) {
			throw new JSONException("BOOM! Error. This is not a valid key value pair.");
		}
		
		//The first result is always a string because it is the key. And it has been checked that it is a string above.
		pair.add(input.get(0).value);
		
		//The parsing for the value after the colon. ( I checked for colon existance above.)
		//This is normally one value but can be an array or object as well which would be multiple values.
		for ( int i = 2; i < input.size(); i++) {
			Symbol currentSymbol = input.get(i);
			switch(currentSymbol.type) {
				case TRUE_BOOLEAN_VALUE :
					pair.add(parseBoolean(currentSymbol));
					break;
				case FALSE_BOOLEAN_VALUE :
					pair.add(parseBoolean(currentSymbol));
					break;
				case NULL_BOOLEAN_VALUE :
					pair.add(parseNull(currentSymbol));
					break;
				case STRING_VALUE :
					pair.add(parseString(currentSymbol));
					break;
				case NUMBER_VALUE :
					pair.add(parseNumber(currentSymbol));
					break;
				//This is a special case. An array is made up of more than one symbol type obviously.
				//Therefore I will have to capture the full size of the array and pass it back to this method
				//hence the recursive nature.
				case LEFT_SQUARE_BRACKET :
					ArrayList<Symbol> newArrayOfSymbols = new ArrayList<Symbol>();
					//Storing the array contents up to the end of the array and then progressing the array reading
					//at this level.
					while ( currentSymbol.type != Symbol.Type.RIGHT_SQUARE_BRACKET ) {
						newArrayOfSymbols.add(currentSymbol);
						i++;
						currentSymbol = input.get(i);
					}
					//The line below will add the right square bracket
					newArrayOfSymbols.add(currentSymbol);
					pair.add(parseArray(newArrayOfSymbols));
					break;
					//The parsing of an object here is similar to an array because they are both
					//multiple symbols contained by a brackets.
				case LEFT_CURLY_BRACKET :
					ArrayList<Symbol> newArrayOfSymbolsForObject = new ArrayList<Symbol>();
					while ( currentSymbol.type != Symbol.Type.RIGHT_CURLY_BRACKET) {
						newArrayOfSymbolsForObject.add(currentSymbol);
						i++;
						currentSymbol = input.get(i);
					}
					newArrayOfSymbolsForObject.add(currentSymbol);
					pair.add(parseObject(newArrayOfSymbolsForObject));
					break;
				default :
					throw new JSONException("BOOM! Error. An object cannot contain this.");
			}
		}
		
		return pair;
	}
	
	/**
	 * This is the method I require to remove the white spaces from the JSON.
	 * @param listOfSymbols
	 * @return ArrayList<Symbol>
	 */
	public ArrayList<Symbol> removeWhiteSpace(ArrayList<Symbol> listOfSymbols) {
		ArrayList<Symbol> newListOfSymbols = new ArrayList<Symbol>();
		for (int i = 0; i < listOfSymbols.size(); i++) {
			if ( listOfSymbols.get(i).type != Symbol.Type.SPACE) {
				newListOfSymbols.add(listOfSymbols.get(i));
			}
		}
		return newListOfSymbols;
	}

}
