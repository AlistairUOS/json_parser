package uk.ac.uos;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.StringReader;

import org.junit.Before;
import org.junit.Test;
import uk.ac.uos.*;

public class LexicalAnalyserTest {

	LexicalAnalyser la;
	
	@Before
	public void setup() {
		la = new LexicalAnalyser(new StringReader(""));
	}
	
	@Test
	public void testGibbersh() throws IOException, JSONException {
		la = new LexicalAnalyser(new StringReader("GIBBERISH"));
		Symbol symbol = la.next();
		assertEquals(null, symbol);
	}
	
	@Test
	public void willStopBeforeEndReached() throws IOException, JSONException {
		la = new LexicalAnalyser(new StringReader("."));
		Symbol symbol = la.next();
		symbol = la.next();
		assertEquals(null, symbol);
	}
	
	@Test
	public void testLeftSquareBracket() throws IOException, JSONException {
		la = new LexicalAnalyser(new StringReader("["));
		Symbol symbol = la.next();
		assertEquals(Symbol.Type.LEFT_SQUARE_BRACKET, symbol.type);
	}
	
	@Test
	public void testLeftCurlyBracket() throws IOException, JSONException {
		la = new LexicalAnalyser(new StringReader("{"));
		Symbol symbol = la.next();
		assertEquals(Symbol.Type.LEFT_CURLY_BRACKET, symbol.type);
	}
	
	@Test
	public void testRightSquareBracket() throws IOException, JSONException {
		la = new LexicalAnalyser(new StringReader("]"));
		Symbol symbol = la.next();
		assertEquals(Symbol.Type.RIGHT_SQUARE_BRACKET, symbol.type);
	}
	
	@Test
	public void testRightCurlyBracket() throws IOException, JSONException {
		la = new LexicalAnalyser(new StringReader("}"));
		Symbol symbol = la.next();
		assertEquals(Symbol.Type.RIGHT_CURLY_BRACKET, symbol.type);
	}
	
	@Test
	public void testTwoCurlyBrackets() throws IOException, JSONException {
		la = new LexicalAnalyser(new StringReader("{}"));
		Symbol symbol = la.next();
		assertEquals(Symbol.Type.LEFT_CURLY_BRACKET, symbol.type);
		symbol = la.next();
		assertEquals(Symbol.Type.RIGHT_CURLY_BRACKET, symbol.type);
	}
	
	@Test
	public void testColon() throws IOException, JSONException {
		la = new LexicalAnalyser(new StringReader(":"));
		Symbol symbol = la.next();
		assertEquals(Symbol.Type.COLON, symbol.type);
	}
	
	@Test
	public void testComma() throws IOException, JSONException {
		la = new LexicalAnalyser(new StringReader(","));
		Symbol symbol = la.next();
		assertEquals(Symbol.Type.COMMA, symbol.type);
	}
	
	@Test
	public void testSpace() throws IOException, JSONException {
		la = new LexicalAnalyser(new StringReader(" "));
		Symbol symbol = la.next();
		assertEquals(Symbol.Type.SPACE, symbol.type);
	}
	
	@Test
	public void testTab() throws IOException, JSONException {
		la = new LexicalAnalyser(new StringReader("	"));
		Symbol symbol = la.next();
		assertEquals(Symbol.Type.SPACE, symbol.type);
	}
	
	@Test
	public void testEnter() throws IOException, JSONException {
		la = new LexicalAnalyser(new StringReader("\n"));
		Symbol symbol = la.next();
		assertEquals(Symbol.Type.SPACE, symbol.type);
	}
	
	@Test
	public void aSymbolAfterSpaceCanBeRead() throws IOException, JSONException {
		la = new LexicalAnalyser(new StringReader(" }"));
		Symbol symbol = la.next();
		assertEquals(Symbol.Type.SPACE, symbol.type);
		symbol = la.next();
		assertEquals(Symbol.Type.RIGHT_CURLY_BRACKET, symbol.type);
	}
	
	@Test
	public void testString() throws IOException, JSONException {
		la = new LexicalAnalyser(new StringReader("\"Hello World\""));
		Symbol symbol = la.next();
		assertEquals(Symbol.Type.STRING_VALUE, symbol.type);
		assertEquals("Hello World", symbol.value);
	}
	
	@Test
	public void testStringInArray() throws IOException, JSONException {
		la = new LexicalAnalyser(new StringReader("[\"Cow\",\"Sheep\"]"));
		Symbol symbol = la.next();
		assertEquals(Symbol.Type.LEFT_SQUARE_BRACKET, symbol.type);
		symbol = la.next();
		assertEquals(Symbol.Type.STRING_VALUE, symbol.type);
		assertEquals("Cow", symbol.value);
		symbol = la.next();
		assertEquals(Symbol.Type.COMMA, symbol.type);
		symbol = la.next();
		assertEquals(Symbol.Type.STRING_VALUE, symbol.type);
		assertEquals("Sheep", symbol.value);
		symbol = la.next();
		assertEquals(Symbol.Type.RIGHT_SQUARE_BRACKET, symbol.type);
	}
	
	@Test
	public void testTrue() throws IOException, JSONException {
		la = new LexicalAnalyser(new StringReader("true"));
		Symbol symbol = la.next();
		assertEquals(Symbol.Type.TRUE_BOOLEAN_VALUE, symbol.type);
	}
	
	@Test
	public void testTruxThrowsException() throws IOException {
		la = new LexicalAnalyser(new StringReader("trux"));
		boolean thrown = false;
		String message = "";
		try {
			la.next();
		} catch (JSONException error) {
			thrown = true;
			message = error.getMessage();
		}
		assertTrue(thrown);
		assertEquals("BOOM! Error. Invalid true value.", message);
	}
	
	@Test
	public void testTrxxThrowsException() throws IOException {
		la = new LexicalAnalyser(new StringReader("trxx"));
		boolean thrown = false;
		String message = "";
		try {
			la.next();
		} catch (JSONException error) {
			thrown = true;
			message = error.getMessage();
		}
		assertTrue(thrown);
		assertEquals("BOOM! Error. Invalid true value.", message);
	}
	
	@Test
	public void testTxxxThrowsException() throws IOException {
		la = new LexicalAnalyser(new StringReader("txxx"));
		boolean thrown = false;
		String message = "";
		try {
			la.next();
		} catch (JSONException error) {
			thrown = true;
			message = error.getMessage();
		}
		assertTrue(thrown);
		assertEquals("BOOM! Error. Invalid true value.", message);
	}
	
	@Test
	public void testNull() throws IOException, JSONException {
		la = new LexicalAnalyser(new StringReader("null"));
		Symbol symbol = la.next();
		assertEquals(Symbol.Type.NULL_BOOLEAN_VALUE, symbol.type);
	}
	
	@Test
	public void testNulxThrowsException() throws IOException {
		la = new LexicalAnalyser(new StringReader("nulx"));
		boolean thrown = false;
		String message = "";
		try {
			la.next();
		} catch (JSONException error) {
			thrown = true;
			message = error.getMessage();
		}
		assertTrue(thrown);
		assertEquals("BOOM! Error. Invalid null value.", message);
	}
	
	@Test
	public void testNuxxThrowsException() throws IOException {
		la = new LexicalAnalyser(new StringReader("nuxx"));
		boolean thrown = false;
		String message = "";
		try {
			la.next();
		} catch (JSONException error) {
			thrown = true;
			message = error.getMessage();
		}
		assertTrue(thrown);
		assertEquals("BOOM! Error. Invalid null value.", message);
	}
	
	@Test
	public void testNxxxThrowsException() throws IOException {
		la = new LexicalAnalyser(new StringReader("nxxx"));
		boolean thrown = false;
		String message = "";
		try {
			la.next();
		} catch (JSONException error) {
			thrown = true;
			message = error.getMessage();
		}
		assertTrue(thrown);
		assertEquals("BOOM! Error. Invalid null value.", message);
	}
	
	@Test
	public void testFalse() throws IOException, JSONException {
		la = new LexicalAnalyser(new StringReader("false"));
		Symbol symbol = la.next();
		assertEquals(Symbol.Type.FALSE_BOOLEAN_VALUE, symbol.type);
	}
	
	@Test
	public void testFalsxThrowsException() throws IOException {
		la = new LexicalAnalyser(new StringReader("falsx"));
		boolean thrown = false;
		String message = "";
		try {
			la.next();
		} catch (JSONException error) {
			thrown = true;
			message = error.getMessage();
		}
		assertTrue(thrown);
		assertEquals("BOOM! Error. Invalid false value.", message);
	}
	
	@Test
	public void testFalxxThrowsException() throws IOException {
		la = new LexicalAnalyser(new StringReader("falxx"));
		boolean thrown = false;
		String message = "";
		try {
			la.next();
		} catch (JSONException error) {
			thrown = true;
			message = error.getMessage();
		}
		assertTrue(thrown);
		assertEquals("BOOM! Error. Invalid false value.", message);
	}
	
	@Test
	public void testFaxxxThrowsException() throws IOException {
		la = new LexicalAnalyser(new StringReader("faxxx"));
		boolean thrown = false;
		String message = "";
		try {
			la.next();
		} catch (JSONException error) {
			thrown = true;
			message = error.getMessage();
		}
		assertTrue(thrown);
		assertEquals("BOOM! Error. Invalid false value.", message);
	}
	
	@Test
	public void testFxxxxThrowsException() throws IOException {
		la = new LexicalAnalyser(new StringReader("fxxxx"));
		boolean thrown = false;
		String message = "";
		try {
			la.next();
		} catch (JSONException error) {
			thrown = true;
			message = error.getMessage();
		}
		assertTrue(thrown);
		assertEquals("BOOM! Error. Invalid false value.", message);
	}
	
	@Test
	public void testInteger() throws IOException, JSONException {
		la = new LexicalAnalyser(new StringReader("4"));
		Symbol symbol = la.next();
		assertEquals(Symbol.Type.NUMBER_VALUE, symbol.type);
	}
	
	@Test
	public void testNegativeInteger() throws IOException, JSONException {
		la = new LexicalAnalyser(new StringReader("-4"));
		Symbol symbol = la.next();
		assertEquals(Symbol.Type.NUMBER_VALUE, symbol.type);
	}
	
	@Test
	public void testFloat() throws IOException, JSONException {
		la = new LexicalAnalyser(new StringReader("4.2"));
		Symbol symbol = la.next();
		assertEquals(Symbol.Type.NUMBER_VALUE, symbol.type);
	}
	
	@Test
	public void testExponentials() throws IOException, JSONException {
		la = new LexicalAnalyser(new StringReader("44E+1"));
		Symbol symbol = la.next();
		assertEquals(Symbol.Type.NUMBER_VALUE, symbol.type);
	}
	
	@Test
	public void testExponentialsLowercaseE() throws IOException, JSONException {
		la = new LexicalAnalyser(new StringReader("44e-1"));
		Symbol symbol = la.next();
		assertEquals(Symbol.Type.NUMBER_VALUE, symbol.type);
	}
	
	@Test
	public void testNumberThenWordInArray() throws IOException, JSONException {
		la = new LexicalAnalyser(new StringReader("[44E+1 , \"Hello\"]"));
		Symbol symbol = la.next();
		assertEquals(Symbol.Type.LEFT_SQUARE_BRACKET, symbol.type);
		symbol = la.next();
		assertEquals(Symbol.Type.NUMBER_VALUE, symbol.type);
		assertEquals("44E+1", symbol.value);
		symbol = la.next();
		assertEquals(Symbol.Type.SPACE, symbol.type);
		symbol = la.next();
		assertEquals(Symbol.Type.COMMA, symbol.type);
		symbol = la.next();
		assertEquals(Symbol.Type.SPACE, symbol.type);
		symbol = la.next();
		assertEquals(Symbol.Type.STRING_VALUE, symbol.type);
		assertEquals("Hello", symbol.value);
	}
	

}
