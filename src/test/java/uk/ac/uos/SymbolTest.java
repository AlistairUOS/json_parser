package uk.ac.uos;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import uk.ac.uos.Symbol.Type;
import uk.ac.uos.*;

public class SymbolTest {
	
	Symbol s;

	@Before
	public void setup() {
		s = new Symbol(null);
	}
	
	@Test
	public void settingAndGettingTheValueAndType() {
		s = new Symbol(Type.STRING_VALUE, "34");
		assertEquals(Type.STRING_VALUE, s.type);
		assertEquals("34", s.value);
	}
	
	@Test
	public void settingAndGettingJustType() {
		s = new Symbol(Type.NUMBER_VALUE);
		assertEquals(Type.NUMBER_VALUE, s.type);
		assertEquals(null, s.value);
	}

}
