package uk.ac.uos;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;
import uk.ac.uos.JSONParser;

public class JSONParserTest {
	
	JSONParser jp;
	
	@Before
	public void setup(){
		jp = new JSONParser();
	}
	
	@Test
	public void parsingAnEmptyObject() throws IOException, JSONException {
		jp.parse("{}");
		assertEquals(new HashMap<String,Object>(), jp.mainObject.objectContents);
	}
	
	@Test
	public void parsingAnEmptyObjectWithSpace() throws IOException, JSONException {
		jp.parse("{ }");
		assertEquals(new HashMap<String,Object>(), jp.mainObject.objectContents);
	}
	
	@Test
	public void parsingAnEmptyObjectWithTab() throws IOException, JSONException {
		jp.parse("{\t}");
		assertEquals(new HashMap<String,Object>(), jp.mainObject.objectContents);
	}
	
	@Test
	public void parsingAnEmptyObjectWithNewline() throws IOException, JSONException {
		jp.parse("{\n}");
		assertEquals(new HashMap<String,Object>(), jp.mainObject.objectContents);
	}
	
	@Test
	public void parsingAStringThatIsCorrect() throws JSONException {
		Symbol symbol = new Symbol(Symbol.Type.STRING_VALUE, "Christmas");
		String result = jp.parseString(symbol);
		assertEquals("Christmas", result);
	}
	
	@Test
	public void parsingAStringThatIsWrong() {
		boolean thrown = false;
		String errorMessage = "";
		Symbol symbol = new Symbol(Symbol.Type.SPACE);
		try {
			jp.parseString(symbol);
		} catch (JSONException error){
			thrown = true;
			errorMessage = error.getMessage();
		}
		assertTrue(thrown);
		assertEquals("BOOM! Error. This is not a string.", errorMessage);
	}
	
	@Test
	public void parsingABooleanTrueThatIsCorrect() throws JSONException {
		Symbol symbol = new Symbol(Symbol.Type.TRUE_BOOLEAN_VALUE);
		boolean result = jp.parseBoolean(symbol);
		assertEquals(true, result);
	}
	
	@Test
	public void parsingABooleanFalseThatIsCorrect() throws JSONException {
		Symbol symbol = new Symbol(Symbol.Type.FALSE_BOOLEAN_VALUE);
		boolean result = jp.parseBoolean(symbol);
		assertEquals(false, result);
	}
	
	@Test
	public void parsingABooleanThatIsNotCorrect() throws JSONException {
		Symbol symbol = new Symbol(Symbol.Type.LEFT_CURLY_BRACKET);
		boolean thrown = false;
		String errorMessage = "";
		try {
			jp.parseBoolean(symbol);
		} catch (JSONException error){
			thrown = true;
			errorMessage = error.getMessage();
		}
		assertTrue(thrown);
		assertEquals("BOOM! Error. This is not a boolean value.", errorMessage);
	}
	
	@Test
	public void parsingANullThatIsCorrect() throws JSONException {
		Symbol symbol = new Symbol(Symbol.Type.NULL_BOOLEAN_VALUE);
		Object result = jp.parseNull(symbol);
		assertEquals(null, result);
	}
	
	@Test
	public void parsingANullThatIsNotCorrect() {
		Symbol symbol = new Symbol(Symbol.Type.STRING_VALUE);
		boolean thrown = false;
		String errorMessage = "";
		try {
			jp.parseNull(symbol);
		} catch (JSONException error) {
			thrown = true;
			errorMessage = error.getMessage();
		}
		assertTrue(thrown);
		assertEquals("BOOM! Error. This is not null.", errorMessage);
	}
	
	@Test
	public void parsingALongThatIsCorrect() throws JSONException {
		Symbol symbol = new Symbol(Symbol.Type.NUMBER_VALUE, "999999");
		long result = (long) jp.parseNumber(symbol);
		assertEquals(999999, result);
	}
	
	@Test
	public void parsingADoubleThatIsCorrect() throws JSONException {
		Symbol symbol = new Symbol(Symbol.Type.NUMBER_VALUE, "5.2");
		double result = (double) jp.parseNumber(symbol);
		double expected = 5.2;
		assertEquals(expected, result, 0);
	}
	
	@Test
	public void parsingANumberWhichIsNotCorrect() throws JSONException {
		Symbol symbol = new Symbol(Symbol.Type.FALSE_BOOLEAN_VALUE);
		boolean thrown = false;
		String errorMessage = "";
		try {
			jp.parseNumber(symbol);
		} catch (JSONException error) {
			thrown = true;
			errorMessage = error.getMessage();
		}
		assertTrue(thrown);
		assertEquals("BOOM! Error. This is not a number.", errorMessage);
	}
	
	@Test
	public void parsingAnEmptyButCorrectArray() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.LEFT_SQUARE_BRACKET);
		Symbol symbol2 = new Symbol(Symbol.Type.RIGHT_SQUARE_BRACKET);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		
		JSONArray jsonArray = jp.parseArray(listOfSymbols);
		assertEquals(0, jsonArray.arrayContents.size());
	}
	
	@Test
	public void parsingAnEmptyIncorrectArray() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.LEFT_CURLY_BRACKET);
		Symbol symbol2 = new Symbol(Symbol.Type.RIGHT_SQUARE_BRACKET);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		
		boolean thrown = false;
		String errorMessage = "";
		
		try {
			jp.parseArray(listOfSymbols);
		} catch (JSONException error) {
			thrown = true;
			errorMessage = error.getMessage();
		}
		
		assertTrue(thrown);
		assertEquals("BOOM! Error. This is not an array.",errorMessage);
	}
	
	@Test
	public void parsingAnIncorrectArrayWrongRightBracket() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.LEFT_SQUARE_BRACKET);
		Symbol symbol2 = new Symbol(Symbol.Type.RIGHT_CURLY_BRACKET);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		
		boolean thrown = false;
		String errorMessage = "";
		
		try {
			jp.parseArray(listOfSymbols);
		} catch (JSONException error) {
			thrown = true;
			errorMessage = error.getMessage();
		}
		
		assertTrue(thrown);
		assertEquals("BOOM! Error. This is not an array.",errorMessage);
	}
	
	@Test
	public void parsingAShortIncorrectArray() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.LEFT_CURLY_BRACKET);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		
		boolean thrown = false;
		String errorMessage = "";
		
		try {
			jp.parseArray(listOfSymbols);
		} catch (JSONException error) {
			thrown = true;
			errorMessage = error.getMessage();
		}
		
		assertTrue(thrown);
		assertEquals("BOOM! Error. This is not an array.",errorMessage);
	}
	
	@Test
	public void parsingAnIncorrectArrayIncorrectSymbolWithin() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.LEFT_SQUARE_BRACKET);
		Symbol symbol2 = new Symbol(Symbol.Type.SPACE);
		Symbol symbol3 = new Symbol(Symbol.Type.RIGHT_SQUARE_BRACKET);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		listOfSymbols.add(symbol3);
		
		boolean thrown = false;
		String errorMessage = "";
		
		try {
			jp.parseArray(listOfSymbols);
		} catch (JSONException error) {
			thrown = true;
			errorMessage = error.getMessage();
		}
		
		assertTrue(thrown);
		assertEquals("BOOM! Error. An array cannot contain this.",errorMessage);
	}
	
	@Test
	public void parsingAnIncorrectArrayMissingComma() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.LEFT_SQUARE_BRACKET);
		Symbol symbol2 = new Symbol(Symbol.Type.STRING_VALUE, "one");
		Symbol symbol3 = new Symbol(Symbol.Type.STRING_VALUE, "two");
		Symbol symbol4 = new Symbol(Symbol.Type.RIGHT_SQUARE_BRACKET);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		listOfSymbols.add(symbol3);
		listOfSymbols.add(symbol4);
		
		boolean thrown = false;
		String errorMessage = "";
		
		try {
			jp.parseArray(listOfSymbols);
		} catch (JSONException error) {
			thrown = true;
			errorMessage = error.getMessage();
		}
		
		assertTrue(thrown);
		assertEquals("BOOM! Error. Missing comma in array.",errorMessage);
	}
	
	@Test
	public void parsingAnArrayWithTrueInIt() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.LEFT_SQUARE_BRACKET);
		Symbol symbol2 = new Symbol(Symbol.Type.TRUE_BOOLEAN_VALUE);
		Symbol symbol3 = new Symbol(Symbol.Type.RIGHT_SQUARE_BRACKET);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		listOfSymbols.add(symbol3);
		
		JSONArray jsonArray = jp.parseArray(listOfSymbols);
		assertEquals(1, jsonArray.arrayContents.size());
		assertEquals(true, jsonArray.arrayContents.get(0));
	}
	
	@Test
	public void parsingAnArrayWithFalseInIt() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.LEFT_SQUARE_BRACKET);
		Symbol symbol2 = new Symbol(Symbol.Type.FALSE_BOOLEAN_VALUE);
		Symbol symbol3 = new Symbol(Symbol.Type.RIGHT_SQUARE_BRACKET);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		listOfSymbols.add(symbol3);
		
		JSONArray jsonArray = jp.parseArray(listOfSymbols);
		assertEquals(1, jsonArray.arrayContents.size());
		assertEquals(false, jsonArray.arrayContents.get(0));
	}
	
	@Test
	public void parsingAnArrayWithNullInIt() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.LEFT_SQUARE_BRACKET);
		Symbol symbol2 = new Symbol(Symbol.Type.NULL_BOOLEAN_VALUE);
		Symbol symbol3 = new Symbol(Symbol.Type.RIGHT_SQUARE_BRACKET);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		listOfSymbols.add(symbol3);
		
		JSONArray jsonArray = jp.parseArray(listOfSymbols);
		assertEquals(1, jsonArray.arrayContents.size());
		assertEquals(null, jsonArray.arrayContents.get(0));
	}
	
	@Test
	public void parsingAnArrayWithAStringInIt() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.LEFT_SQUARE_BRACKET);
		Symbol symbol2 = new Symbol(Symbol.Type.STRING_VALUE, "Merry Christmas");
		Symbol symbol3 = new Symbol(Symbol.Type.RIGHT_SQUARE_BRACKET);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		listOfSymbols.add(symbol3);
		
		JSONArray jsonArray = jp.parseArray(listOfSymbols);
		assertEquals(1, jsonArray.arrayContents.size());
		assertEquals("Merry Christmas", jsonArray.arrayContents.get(0));
	}
	
	@Test
	public void parsingAnArrayWithALongInIt() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.LEFT_SQUARE_BRACKET);
		Symbol symbol2 = new Symbol(Symbol.Type.NUMBER_VALUE, "1337");
		Symbol symbol3 = new Symbol(Symbol.Type.RIGHT_SQUARE_BRACKET);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		listOfSymbols.add(symbol3);
		
		JSONArray jsonArray = jp.parseArray(listOfSymbols);
		assertEquals(1, jsonArray.arrayContents.size());
		assertEquals((long) 1337, jsonArray.arrayContents.get(0));
	}
	
	@Test
	public void parsingAnArrayWithADoubleInIt() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.LEFT_SQUARE_BRACKET);
		Symbol symbol2 = new Symbol(Symbol.Type.NUMBER_VALUE, "13.37");
		Symbol symbol3 = new Symbol(Symbol.Type.RIGHT_SQUARE_BRACKET);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		listOfSymbols.add(symbol3);
		
		JSONArray jsonArray = jp.parseArray(listOfSymbols);
		assertEquals(1, jsonArray.arrayContents.size());
		assertEquals((double) 13.37, jsonArray.arrayContents.get(0));
	}
	
	@Test
	public void parsingAnArrayWithTwoValuesIn() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.LEFT_SQUARE_BRACKET);
		Symbol symbol2 = new Symbol(Symbol.Type.TRUE_BOOLEAN_VALUE);
		Symbol symbol3 = new Symbol(Symbol.Type.COMMA);
		Symbol symbol4 = new Symbol(Symbol.Type.FALSE_BOOLEAN_VALUE);
		Symbol symbol5 = new Symbol(Symbol.Type.RIGHT_SQUARE_BRACKET);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		listOfSymbols.add(symbol3);
		listOfSymbols.add(symbol4);
		listOfSymbols.add(symbol5);
		
		JSONArray jsonArray = jp.parseArray(listOfSymbols);
		assertEquals(2, jsonArray.arrayContents.size());
		assertEquals(true, jsonArray.arrayContents.get(0));
		assertEquals(false, jsonArray.arrayContents.get(1));
	}
	
	@Test
	public void parsingAnArrayWithEveryValueIn() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.LEFT_SQUARE_BRACKET);
		Symbol symbol2 = new Symbol(Symbol.Type.TRUE_BOOLEAN_VALUE);
		Symbol symbol3 = new Symbol(Symbol.Type.COMMA);
		Symbol symbol4 = new Symbol(Symbol.Type.FALSE_BOOLEAN_VALUE);
		Symbol symbol5 = new Symbol(Symbol.Type.COMMA);
		Symbol symbol6 = new Symbol(Symbol.Type.NULL_BOOLEAN_VALUE);
		Symbol symbol7 = new Symbol(Symbol.Type.COMMA);
		Symbol symbol8 = new Symbol(Symbol.Type.STRING_VALUE, "TEST");
		Symbol symbol9 = new Symbol(Symbol.Type.COMMA);
		Symbol symbol10 = new Symbol(Symbol.Type.NUMBER_VALUE, "46");
		Symbol symbol11 = new Symbol(Symbol.Type.COMMA);
		Symbol symbol12 = new Symbol(Symbol.Type.NUMBER_VALUE, "1.2");
		Symbol symbol13 = new Symbol(Symbol.Type.RIGHT_SQUARE_BRACKET);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		listOfSymbols.add(symbol3);
		listOfSymbols.add(symbol4);
		listOfSymbols.add(symbol5);
		listOfSymbols.add(symbol6);
		listOfSymbols.add(symbol7);
		listOfSymbols.add(symbol8);
		listOfSymbols.add(symbol9);
		listOfSymbols.add(symbol10);
		listOfSymbols.add(symbol11);
		listOfSymbols.add(symbol12);
		listOfSymbols.add(symbol13);
		
		JSONArray jsonArray = jp.parseArray(listOfSymbols);
		assertEquals(6, jsonArray.arrayContents.size());
		assertEquals(true, jsonArray.arrayContents.get(0));
		assertEquals(false, jsonArray.arrayContents.get(1));
		assertEquals(null, jsonArray.arrayContents.get(2));
		assertEquals("TEST", jsonArray.arrayContents.get(3));
		assertEquals((long) 46, jsonArray.arrayContents.get(4));
		assertEquals(1.2, jsonArray.arrayContents.get(5));
	}
	
	@Test
	public void parsingAnArrayContainingAnArray() throws JSONException { 
		Symbol symbol1 = new Symbol(Symbol.Type.LEFT_SQUARE_BRACKET);
		Symbol symbol2 = new Symbol(Symbol.Type.LEFT_SQUARE_BRACKET);
		Symbol symbol3 = new Symbol(Symbol.Type.RIGHT_SQUARE_BRACKET);
		Symbol symbol4 = new Symbol(Symbol.Type.RIGHT_SQUARE_BRACKET);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		listOfSymbols.add(symbol3);
		listOfSymbols.add(symbol4);
		
		JSONArray jsonArray = jp.parseArray(listOfSymbols);
		assertEquals(1, jsonArray.arrayContents.size());
		JSONArray tierTwoArray = (JSONArray) jsonArray.arrayContents.get(0);
		assertEquals(0, tierTwoArray.arrayContents.size());
	}
	
	@Test
	public void parsingAnArrayContainingAnArrayWhichHasATrueInIt() throws JSONException { 
		Symbol symbol1 = new Symbol(Symbol.Type.LEFT_SQUARE_BRACKET);
		Symbol symbol2 = new Symbol(Symbol.Type.LEFT_SQUARE_BRACKET);
		Symbol symbol3 = new Symbol(Symbol.Type.TRUE_BOOLEAN_VALUE);
		Symbol symbol4 = new Symbol(Symbol.Type.RIGHT_SQUARE_BRACKET);
		Symbol symbol5 = new Symbol(Symbol.Type.RIGHT_SQUARE_BRACKET);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		listOfSymbols.add(symbol3);
		listOfSymbols.add(symbol4);
		listOfSymbols.add(symbol5);
		
		JSONArray jsonArray = jp.parseArray(listOfSymbols);
		assertEquals(1, jsonArray.arrayContents.size());
		JSONArray tierTwoArray = (JSONArray) jsonArray.arrayContents.get(0);
		assertEquals(1, tierTwoArray.arrayContents.size());
		assertEquals(true, tierTwoArray.arrayContents.get(0));
	}
	
	@Test
	public void parsingAnArrayContainingAnArrayWhichHasAFalseInIt() throws JSONException { 
		Symbol symbol1 = new Symbol(Symbol.Type.LEFT_SQUARE_BRACKET);
		Symbol symbol2 = new Symbol(Symbol.Type.LEFT_SQUARE_BRACKET);
		Symbol symbol3 = new Symbol(Symbol.Type.FALSE_BOOLEAN_VALUE);
		Symbol symbol4 = new Symbol(Symbol.Type.RIGHT_SQUARE_BRACKET);
		Symbol symbol5 = new Symbol(Symbol.Type.RIGHT_SQUARE_BRACKET);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		listOfSymbols.add(symbol3);
		listOfSymbols.add(symbol4);
		listOfSymbols.add(symbol5);
		
		JSONArray jsonArray = jp.parseArray(listOfSymbols);
		assertEquals(1, jsonArray.arrayContents.size());
		JSONArray tierTwoArray = (JSONArray) jsonArray.arrayContents.get(0);
		assertEquals(1, tierTwoArray.arrayContents.size());
		assertEquals(false, tierTwoArray.arrayContents.get(0));
	}
	
	@Test
	public void parsingAnArrayContainingAnArrayWhichHasTwoStringsInIt() throws JSONException { 
		Symbol symbol1 = new Symbol(Symbol.Type.LEFT_SQUARE_BRACKET);
		Symbol symbol2 = new Symbol(Symbol.Type.LEFT_SQUARE_BRACKET);
		Symbol symbol3 = new Symbol(Symbol.Type.STRING_VALUE, "string1");
		Symbol symbol4 = new Symbol(Symbol.Type.COMMA);
		Symbol symbol5 = new Symbol(Symbol.Type.STRING_VALUE, "string2");
		Symbol symbol6 = new Symbol(Symbol.Type.RIGHT_SQUARE_BRACKET);
		Symbol symbol7 = new Symbol(Symbol.Type.RIGHT_SQUARE_BRACKET);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		listOfSymbols.add(symbol3);
		listOfSymbols.add(symbol4);
		listOfSymbols.add(symbol5);
		listOfSymbols.add(symbol6);
		listOfSymbols.add(symbol7);
		
		JSONArray jsonArray = jp.parseArray(listOfSymbols);
		assertEquals(1, jsonArray.arrayContents.size());
		JSONArray tierTwoArray = (JSONArray) jsonArray.arrayContents.get(0);
		assertEquals(2, tierTwoArray.arrayContents.size());
		assertEquals("string1", tierTwoArray.arrayContents.get(0));
		assertEquals("string2", tierTwoArray.arrayContents.get(1));
	}
	
	@Test
	public void parsingAnArrayContainingAnArrayWhichHasTwoStringsInItAndANull() throws JSONException { 
		Symbol symbol1 = new Symbol(Symbol.Type.LEFT_SQUARE_BRACKET);
		Symbol symbol2 = new Symbol(Symbol.Type.LEFT_SQUARE_BRACKET);
		Symbol symbol3 = new Symbol(Symbol.Type.STRING_VALUE, "string1");
		Symbol symbol4 = new Symbol(Symbol.Type.COMMA);
		Symbol symbol5 = new Symbol(Symbol.Type.STRING_VALUE, "string2");
		Symbol symbol6 = new Symbol(Symbol.Type.RIGHT_SQUARE_BRACKET);
		Symbol symbol7 = new Symbol(Symbol.Type.COMMA);
		Symbol symbol8 = new Symbol(Symbol.Type.NULL_BOOLEAN_VALUE);
		Symbol symbol9 = new Symbol(Symbol.Type.RIGHT_SQUARE_BRACKET);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		listOfSymbols.add(symbol3);
		listOfSymbols.add(symbol4);
		listOfSymbols.add(symbol5);
		listOfSymbols.add(symbol6);
		listOfSymbols.add(symbol7);
		listOfSymbols.add(symbol8);
		listOfSymbols.add(symbol9);
		
		JSONArray jsonArray = jp.parseArray(listOfSymbols);
		assertEquals(2, jsonArray.arrayContents.size());
		JSONArray tierTwoArray = (JSONArray) jsonArray.arrayContents.get(0);
		assertEquals(2, tierTwoArray.arrayContents.size());
		assertEquals("string1", tierTwoArray.arrayContents.get(0));
		assertEquals("string2", tierTwoArray.arrayContents.get(1));
		assertEquals(null, jsonArray.arrayContents.get(1));
	}
	
	@Test
	public void parsingAnEmptyButCorrectObject() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.LEFT_CURLY_BRACKET);
		Symbol symbol2 = new Symbol(Symbol.Type.RIGHT_CURLY_BRACKET);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		
		JSONObject jsonArray = jp.parseObject(listOfSymbols);
		assertEquals(0, jsonArray.objectContents.size());
	}
	
	@Test
	public void parsingAnEmptyIncorrectObject() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.LEFT_CURLY_BRACKET);
		Symbol symbol2 = new Symbol(Symbol.Type.RIGHT_SQUARE_BRACKET);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		
		boolean thrown = false;
		String errorMessage = "";
		
		try {
			jp.parseObject(listOfSymbols);
		} catch (JSONException error) {
			thrown = true;
			errorMessage = error.getMessage();
		}
		
		assertTrue(thrown);
		assertEquals("BOOM! Error. This is not an object.",errorMessage);
	}
	
	@Test
	public void parsingAnEmptyIncorrectObjectStartsWrong() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.LEFT_SQUARE_BRACKET);
		Symbol symbol2 = new Symbol(Symbol.Type.RIGHT_CURLY_BRACKET);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		
		boolean thrown = false;
		String errorMessage = "";
		
		try {
			jp.parseObject(listOfSymbols);
		} catch (JSONException error) {
			thrown = true;
			errorMessage = error.getMessage();
		}
		
		assertTrue(thrown);
		assertEquals("BOOM! Error. This is not an object.",errorMessage);
	}
	
	@Test
	public void parsingAnEmptyIncorrectObjectWithMissingComma() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.LEFT_CURLY_BRACKET);
		Symbol symbol2 = new Symbol(Symbol.Type.STRING_VALUE, "Hey");
		Symbol symbol3 = new Symbol(Symbol.Type.COLON);
		Symbol symbol4 = new Symbol(Symbol.Type.NUMBER_VALUE, "9");
		Symbol symbol5 = new Symbol(Symbol.Type.STRING_VALUE, "Good");
		Symbol symbol6 = new Symbol(Symbol.Type.COLON);
		Symbol symbol7 = new Symbol(Symbol.Type.NULL_BOOLEAN_VALUE);
		Symbol symbol8 = new Symbol(Symbol.Type.RIGHT_CURLY_BRACKET);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		listOfSymbols.add(symbol3);
		listOfSymbols.add(symbol4);
		listOfSymbols.add(symbol5);
		listOfSymbols.add(symbol6);
		listOfSymbols.add(symbol7);
		listOfSymbols.add(symbol8);
		
		boolean thrown = false;
		String errorMessage = "";
		
		try {
			jp.parseObject(listOfSymbols);
		} catch (JSONException error) {
			thrown = true;
			errorMessage = error.getMessage();
		}
		
		assertTrue(thrown);
		assertEquals("BOOM! Error. Missing comma in object.",errorMessage);
	}
	
	@Test
	public void parsingAShortIncorrectObject() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.LEFT_CURLY_BRACKET);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		
		boolean thrown = false;
		String errorMessage = "";
		
		try {
			jp.parseObject(listOfSymbols);
		} catch (JSONException error) {
			thrown = true;
			errorMessage = error.getMessage();
		}
		
		assertTrue(thrown);
		assertEquals("BOOM! Error. This is not an object.",errorMessage);
	}
	
	@Test
	public void parsingATrueValuePairThatsTooShort() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.STRING_VALUE, "KEY");
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		
		boolean thrown = false;
		String errorMessage = "";
		
		try {
			jp.parseKeyValuePair(listOfSymbols);
		} catch (JSONException error) {
			thrown = true;
			errorMessage = error.getMessage();
		}
		
		assertTrue(thrown);
		assertEquals("BOOM! Error. This is not a valid key value pair.",errorMessage);
	}
	
	@Test
	public void parsingATrueValuePairWithoutColon() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.STRING_VALUE, "KEY");
		Symbol symbol2 = new Symbol(Symbol.Type.TRUE_BOOLEAN_VALUE);
		Symbol symbol3 = new Symbol(Symbol.Type.TRUE_BOOLEAN_VALUE);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		listOfSymbols.add(symbol3);
		
		boolean thrown = false;
		String errorMessage = "";
		
		try {
			jp.parseKeyValuePair(listOfSymbols);
		} catch (JSONException error) {
			thrown = true;
			errorMessage = error.getMessage();
		}
		
		assertTrue(thrown);
		assertEquals("BOOM! Error. This is not a valid key value pair.",errorMessage);
	}
	
	@Test
	public void parsingAPairThatHasAnIncorrectValue() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.STRING_VALUE, "KEY");
		Symbol symbol2 = new Symbol(Symbol.Type.COLON);
		Symbol symbol3 = new Symbol(Symbol.Type.SPACE);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		listOfSymbols.add(symbol3);
		
		boolean thrown = false;
		String errorMessage = "";
		
		try {
			jp.parseKeyValuePair(listOfSymbols);
		} catch (JSONException error) {
			thrown = true;
			errorMessage = error.getMessage();
		}
		
		assertTrue(thrown);
		assertEquals("BOOM! Error. An object cannot contain this.",errorMessage);
	}
	
	@Test
	public void parsingATrueValuePairWithoutStringThrowsException() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.NULL_BOOLEAN_VALUE);
		Symbol symbol2 = new Symbol(Symbol.Type.COLON);
		Symbol symbol3 = new Symbol(Symbol.Type.TRUE_BOOLEAN_VALUE);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		listOfSymbols.add(symbol3);
		
		boolean thrown = false;
		String errorMessage = "";
		
		try {
			jp.parseKeyValuePair(listOfSymbols);
		} catch (JSONException error) {
			thrown = true;
			errorMessage = error.getMessage();
		}
		
		assertTrue(thrown);
		assertEquals("BOOM! Error. This is not a valid key value pair.",errorMessage);
	}
	
	@Test
	public void parsingATrueValuePair() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.STRING_VALUE, "KEY");
		Symbol symbol2 = new Symbol(Symbol.Type.COLON);
		Symbol symbol3 = new Symbol(Symbol.Type.TRUE_BOOLEAN_VALUE);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		listOfSymbols.add(symbol3);
		
		ArrayList<Object> keyValuePair = jp.parseKeyValuePair(listOfSymbols);
		assertEquals(2, keyValuePair.size());
		assertEquals("KEY", keyValuePair.get(0));
		assertEquals(true, keyValuePair.get(1));
	}
	
	@Test
	public void parsingAFalseValuePair() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.STRING_VALUE, "KEY");
		Symbol symbol2 = new Symbol(Symbol.Type.COLON);
		Symbol symbol3 = new Symbol(Symbol.Type.FALSE_BOOLEAN_VALUE);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		listOfSymbols.add(symbol3);
		
		ArrayList<Object> keyValuePair = jp.parseKeyValuePair(listOfSymbols);
		assertEquals(2, keyValuePair.size());
		assertEquals("KEY", keyValuePair.get(0));
		assertEquals(false, keyValuePair.get(1));
	}
	
	@Test
	public void parsingANullValuePair() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.STRING_VALUE, "KEY");
		Symbol symbol2 = new Symbol(Symbol.Type.COLON);
		Symbol symbol3 = new Symbol(Symbol.Type.NULL_BOOLEAN_VALUE);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		listOfSymbols.add(symbol3);
		
		ArrayList<Object> keyValuePair = jp.parseKeyValuePair(listOfSymbols);
		assertEquals(2, keyValuePair.size());
		assertEquals("KEY", keyValuePair.get(0));
		assertEquals(null, keyValuePair.get(1));
	}
	
	@Test
	public void parsingAStringValuePair() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.STRING_VALUE, "KEY");
		Symbol symbol2 = new Symbol(Symbol.Type.COLON);
		Symbol symbol3 = new Symbol(Symbol.Type.STRING_VALUE, "value");
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		listOfSymbols.add(symbol3);
		
		ArrayList<Object> keyValuePair = jp.parseKeyValuePair(listOfSymbols);
		assertEquals(2, keyValuePair.size());
		assertEquals("KEY", keyValuePair.get(0));
		assertEquals("value", keyValuePair.get(1));
	}
	
	@Test
	public void parsingALongValuePair() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.STRING_VALUE, "KEY");
		Symbol symbol2 = new Symbol(Symbol.Type.COLON);
		Symbol symbol3 = new Symbol(Symbol.Type.NUMBER_VALUE, "10661066");
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		listOfSymbols.add(symbol3);
		
		ArrayList<Object> keyValuePair = jp.parseKeyValuePair(listOfSymbols);
		assertEquals(2, keyValuePair.size());
		assertEquals("KEY", keyValuePair.get(0));
		assertEquals((long) 10661066, keyValuePair.get(1));
	}
	
	@Test
	public void parsingADoubleValuePair() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.STRING_VALUE, "KEY");
		Symbol symbol2 = new Symbol(Symbol.Type.COLON);
		Symbol symbol3 = new Symbol(Symbol.Type.NUMBER_VALUE, "1066.1066");
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		listOfSymbols.add(symbol3);
		
		ArrayList<Object> keyValuePair = jp.parseKeyValuePair(listOfSymbols);
		assertEquals(2, keyValuePair.size());
		assertEquals("KEY", keyValuePair.get(0));
		assertEquals((double) 1066.1066, keyValuePair.get(1));
	}
	
	@Test
	public void parsingAnEmptyArrayValuePair() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.STRING_VALUE, "KEY");
		Symbol symbol2 = new Symbol(Symbol.Type.COLON);
		Symbol symbol3 = new Symbol(Symbol.Type.LEFT_SQUARE_BRACKET);
		Symbol symbol4 = new Symbol(Symbol.Type.RIGHT_SQUARE_BRACKET);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		listOfSymbols.add(symbol3);
		listOfSymbols.add(symbol4);
		
		ArrayList<Object> keyValuePair = jp.parseKeyValuePair(listOfSymbols);
		assertEquals(2, keyValuePair.size());
		assertEquals("KEY", keyValuePair.get(0));
		JSONArray secondTierArray = (JSONArray) keyValuePair.get(1);
		assertEquals(0, secondTierArray.arrayContents.size());
	}
	
	@Test
	public void parsingAnArrayWithTrueValuePair() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.STRING_VALUE, "KEY");
		Symbol symbol2 = new Symbol(Symbol.Type.COLON);
		Symbol symbol3 = new Symbol(Symbol.Type.LEFT_SQUARE_BRACKET);
		Symbol symbol4 = new Symbol(Symbol.Type.TRUE_BOOLEAN_VALUE);
		Symbol symbol5 = new Symbol(Symbol.Type.RIGHT_SQUARE_BRACKET);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		listOfSymbols.add(symbol3);
		listOfSymbols.add(symbol4);
		listOfSymbols.add(symbol5);
		
		ArrayList<Object> keyValuePair = jp.parseKeyValuePair(listOfSymbols);
		assertEquals(2, keyValuePair.size());
		assertEquals("KEY", keyValuePair.get(0));
		JSONArray secondTierArray = (JSONArray) keyValuePair.get(1);
		assertEquals(1, secondTierArray.arrayContents.size());
		assertEquals(true, secondTierArray.arrayContents.get(0));
	}
	
	@Test
	public void parsingAnObjectWithATrueValue() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.LEFT_CURLY_BRACKET);
		Symbol symbol2 = new Symbol(Symbol.Type.STRING_VALUE, "NICE KEY");
		Symbol symbol3 = new Symbol(Symbol.Type.COLON);
		Symbol symbol4 = new Symbol(Symbol.Type.TRUE_BOOLEAN_VALUE);
		Symbol symbol5 = new Symbol(Symbol.Type.RIGHT_CURLY_BRACKET);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		listOfSymbols.add(symbol3);
		listOfSymbols.add(symbol4);
		listOfSymbols.add(symbol5);
		
		JSONObject jsonObject = jp.parseObject(listOfSymbols);
		assertEquals(1, jsonObject.objectContents.size());
		assertEquals(true, jsonObject.objectContents.get("NICE KEY"));
	}
	
	@Test
	public void parsingAnObjectWithAFalseValue() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.LEFT_CURLY_BRACKET);
		Symbol symbol2 = new Symbol(Symbol.Type.STRING_VALUE, "WHAT A NICE KEY");
		Symbol symbol3 = new Symbol(Symbol.Type.COLON);
		Symbol symbol4 = new Symbol(Symbol.Type.FALSE_BOOLEAN_VALUE);
		Symbol symbol5 = new Symbol(Symbol.Type.RIGHT_CURLY_BRACKET);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		listOfSymbols.add(symbol3);
		listOfSymbols.add(symbol4);
		listOfSymbols.add(symbol5);
		
		JSONObject jsonObject = jp.parseObject(listOfSymbols);
		assertEquals(1, jsonObject.objectContents.size());
		assertEquals(false, jsonObject.objectContents.get("WHAT A NICE KEY"));
	}
	
	@Test
	public void parsingAnObjectWithANullValue() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.LEFT_CURLY_BRACKET);
		Symbol symbol2 = new Symbol(Symbol.Type.STRING_VALUE, "");
		Symbol symbol3 = new Symbol(Symbol.Type.COLON);
		Symbol symbol4 = new Symbol(Symbol.Type.NULL_BOOLEAN_VALUE);
		Symbol symbol5 = new Symbol(Symbol.Type.RIGHT_CURLY_BRACKET);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		listOfSymbols.add(symbol3);
		listOfSymbols.add(symbol4);
		listOfSymbols.add(symbol5);
		
		JSONObject jsonObject = jp.parseObject(listOfSymbols);
		assertEquals(1, jsonObject.objectContents.size());
		assertEquals(null, jsonObject.objectContents.get(""));
	}
	
	@Test
	public void parsingAnObjectWithAStringValue() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.LEFT_CURLY_BRACKET);
		Symbol symbol2 = new Symbol(Symbol.Type.STRING_VALUE, "okay");
		Symbol symbol3 = new Symbol(Symbol.Type.COLON);
		Symbol symbol4 = new Symbol(Symbol.Type.STRING_VALUE, "hiya!");
		Symbol symbol5 = new Symbol(Symbol.Type.RIGHT_CURLY_BRACKET);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		listOfSymbols.add(symbol3);
		listOfSymbols.add(symbol4);
		listOfSymbols.add(symbol5);
		
		JSONObject jsonObject = jp.parseObject(listOfSymbols);
		assertEquals(1, jsonObject.objectContents.size());
		assertEquals("hiya!", jsonObject.objectContents.get("okay"));
	}
	
	@Test
	public void parsingAnObjectWithALongValue() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.LEFT_CURLY_BRACKET);
		Symbol symbol2 = new Symbol(Symbol.Type.STRING_VALUE, "okay");
		Symbol symbol3 = new Symbol(Symbol.Type.COLON);
		Symbol symbol4 = new Symbol(Symbol.Type.NUMBER_VALUE, "999999");
		Symbol symbol5 = new Symbol(Symbol.Type.RIGHT_CURLY_BRACKET);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		listOfSymbols.add(symbol3);
		listOfSymbols.add(symbol4);
		listOfSymbols.add(symbol5);
		
		JSONObject jsonObject = jp.parseObject(listOfSymbols);
		assertEquals(1, jsonObject.objectContents.size());
		assertEquals((long) 999999, jsonObject.objectContents.get("okay"));
	}
	
	@Test
	public void parsingAnObjectWithADoubleValue() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.LEFT_CURLY_BRACKET);
		Symbol symbol2 = new Symbol(Symbol.Type.STRING_VALUE, "okay");
		Symbol symbol3 = new Symbol(Symbol.Type.COLON);
		Symbol symbol4 = new Symbol(Symbol.Type.NUMBER_VALUE, "999.999");
		Symbol symbol5 = new Symbol(Symbol.Type.RIGHT_CURLY_BRACKET);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		listOfSymbols.add(symbol3);
		listOfSymbols.add(symbol4);
		listOfSymbols.add(symbol5);
		
		JSONObject jsonObject = jp.parseObject(listOfSymbols);
		assertEquals(1, jsonObject.objectContents.size());
		assertEquals((double) 999.999, jsonObject.objectContents.get("okay"));
	}
	
	@Test
	public void parsingAnObjectWithAnArrayValue() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.LEFT_CURLY_BRACKET);
		Symbol symbol2 = new Symbol(Symbol.Type.STRING_VALUE, "okay");
		Symbol symbol3 = new Symbol(Symbol.Type.COLON);
		Symbol symbol4 = new Symbol(Symbol.Type.LEFT_SQUARE_BRACKET);
		Symbol symbol5 = new Symbol(Symbol.Type.RIGHT_SQUARE_BRACKET);
		Symbol symbol6 = new Symbol(Symbol.Type.RIGHT_CURLY_BRACKET);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		listOfSymbols.add(symbol3);
		listOfSymbols.add(symbol4);
		listOfSymbols.add(symbol5);
		listOfSymbols.add(symbol6);
		
		JSONObject jsonObject = jp.parseObject(listOfSymbols);
		assertEquals(1, jsonObject.objectContents.size());
		JSONArray secondTierArray = (JSONArray) jsonObject.objectContents.get("okay");
		assertEquals(0, secondTierArray.arrayContents.size());
	}
	
	@Test
	public void parsingAnObjectWithAnArrayValueWithAStringInIt() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.LEFT_CURLY_BRACKET);
		Symbol symbol2 = new Symbol(Symbol.Type.STRING_VALUE, "okay");
		Symbol symbol3 = new Symbol(Symbol.Type.COLON);
		Symbol symbol4 = new Symbol(Symbol.Type.LEFT_SQUARE_BRACKET);
		Symbol symbol5 = new Symbol(Symbol.Type.STRING_VALUE, "I am here!");
		Symbol symbol6 = new Symbol(Symbol.Type.RIGHT_SQUARE_BRACKET);
		Symbol symbol7 = new Symbol(Symbol.Type.RIGHT_CURLY_BRACKET);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		listOfSymbols.add(symbol3);
		listOfSymbols.add(symbol4);
		listOfSymbols.add(symbol5);
		listOfSymbols.add(symbol6);
		listOfSymbols.add(symbol7);
		
		JSONObject jsonObject = jp.parseObject(listOfSymbols);
		assertEquals(1, jsonObject.objectContents.size());
		JSONArray secondTierArray = (JSONArray) jsonObject.objectContents.get("okay");
		assertEquals(1, secondTierArray.arrayContents.size());
		assertEquals("I am here!", secondTierArray.arrayContents.get(0));
	}
	
	@Test
	public void parsingAnObjectWithAnObjectInIt() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.LEFT_CURLY_BRACKET);
		Symbol symbol2 = new Symbol(Symbol.Type.STRING_VALUE, "object");
		Symbol symbol3 = new Symbol(Symbol.Type.COLON);
		Symbol symbol4 = new Symbol(Symbol.Type.LEFT_CURLY_BRACKET);
		Symbol symbol5 = new Symbol(Symbol.Type.RIGHT_CURLY_BRACKET);
		Symbol symbol6 = new Symbol(Symbol.Type.RIGHT_CURLY_BRACKET);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		listOfSymbols.add(symbol3);
		listOfSymbols.add(symbol4);
		listOfSymbols.add(symbol5);
		listOfSymbols.add(symbol6);
		
		JSONObject jsonObject = jp.parseObject(listOfSymbols);
		assertEquals(1, jsonObject.objectContents.size());
		JSONObject secondTierObject = (JSONObject) jsonObject.objectContents.get("object");
		assertEquals(0, secondTierObject.objectContents.size());
	}
	
	@Test
	public void parsingAnObjectWithAnObjectAndFalseInIt() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.LEFT_CURLY_BRACKET);
		Symbol symbol2 = new Symbol(Symbol.Type.STRING_VALUE, "object");
		Symbol symbol3 = new Symbol(Symbol.Type.COLON);
		Symbol symbol4 = new Symbol(Symbol.Type.LEFT_CURLY_BRACKET);
		Symbol symbol5 = new Symbol(Symbol.Type.STRING_VALUE, "false");
		Symbol symbol6 = new Symbol(Symbol.Type.COLON);
		Symbol symbol7 = new Symbol(Symbol.Type.FALSE_BOOLEAN_VALUE);
		Symbol symbol8 = new Symbol(Symbol.Type.RIGHT_CURLY_BRACKET);
		Symbol symbol9 = new Symbol(Symbol.Type.RIGHT_CURLY_BRACKET);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		listOfSymbols.add(symbol3);
		listOfSymbols.add(symbol4);
		listOfSymbols.add(symbol5);
		listOfSymbols.add(symbol6);
		listOfSymbols.add(symbol7);
		listOfSymbols.add(symbol8);
		listOfSymbols.add(symbol9);
		
		JSONObject jsonObject = jp.parseObject(listOfSymbols);
		assertEquals(1, jsonObject.objectContents.size());
		JSONObject secondTierObject = (JSONObject) jsonObject.objectContents.get("object");
		assertEquals(1, secondTierObject.objectContents.size());
		assertEquals(false, secondTierObject.objectContents.get("false"));
	}
	
	@Test
	public void parsingAnArrayWithAnObjectInIt() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.LEFT_SQUARE_BRACKET);
		Symbol symbol2 = new Symbol(Symbol.Type.LEFT_CURLY_BRACKET);
		Symbol symbol3 = new Symbol(Symbol.Type.STRING_VALUE, "false");
		Symbol symbol4 = new Symbol(Symbol.Type.COLON);
		Symbol symbol5 = new Symbol(Symbol.Type.FALSE_BOOLEAN_VALUE);
		Symbol symbol6 = new Symbol(Symbol.Type.RIGHT_CURLY_BRACKET);
		Symbol symbol7 = new Symbol(Symbol.Type.RIGHT_SQUARE_BRACKET);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		listOfSymbols.add(symbol3);
		listOfSymbols.add(symbol4);
		listOfSymbols.add(symbol5);
		listOfSymbols.add(symbol6);
		listOfSymbols.add(symbol7);
		
		JSONArray jsonArray = jp.parseArray(listOfSymbols);
		assertEquals(1, jsonArray.arrayContents.size());
		JSONObject jsonObject = (JSONObject) jsonArray.arrayContents.get(0);
		assertEquals(1, jsonObject.objectContents.size());
		assertEquals(false, jsonObject.objectContents.get("false"));
	}
	
	@Test
	public void parsingAnObjectWithATwoStrings() throws JSONException {
		Symbol symbol1 = new Symbol(Symbol.Type.LEFT_CURLY_BRACKET);
		Symbol symbol2 = new Symbol(Symbol.Type.STRING_VALUE, "Name");
		Symbol symbol3 = new Symbol(Symbol.Type.COLON);
		Symbol symbol4 = new Symbol(Symbol.Type.STRING_VALUE, "Alistair");
		Symbol symbol5 = new Symbol(Symbol.Type.COMMA);
		Symbol symbol6 = new Symbol(Symbol.Type.STRING_VALUE, "Age");
		Symbol symbol7 = new Symbol(Symbol.Type.COLON);
		Symbol symbol8 = new Symbol(Symbol.Type.STRING_VALUE, "20");
		Symbol symbol9 = new Symbol(Symbol.Type.RIGHT_CURLY_BRACKET);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		listOfSymbols.add(symbol3);
		listOfSymbols.add(symbol4);
		listOfSymbols.add(symbol5);
		listOfSymbols.add(symbol6);
		listOfSymbols.add(symbol7);
		listOfSymbols.add(symbol8);
		listOfSymbols.add(symbol9);
		
		JSONObject jsonObject = jp.parseObject(listOfSymbols);
		assertEquals(2, jsonObject.objectContents.size());
		assertEquals("Alistair", jsonObject.objectContents.get("Name"));
		assertEquals("20", jsonObject.objectContents.get("Age"));
	}
	
	@Test
	public void canRemoveWhiteSpace() {
		Symbol symbol1 = new Symbol(Symbol.Type.LEFT_CURLY_BRACKET);
		Symbol symbol2 = new Symbol(Symbol.Type.SPACE);
		Symbol symbol3 = new Symbol(Symbol.Type.SPACE);
		Symbol symbol4 = new Symbol(Symbol.Type.SPACE);
		Symbol symbol5 = new Symbol(Symbol.Type.SPACE);
		Symbol symbol6 = new Symbol(Symbol.Type.RIGHT_CURLY_BRACKET);
		ArrayList<Symbol> listOfSymbols = new ArrayList<Symbol>();
		listOfSymbols.add(symbol1);
		listOfSymbols.add(symbol2);
		listOfSymbols.add(symbol3);
		listOfSymbols.add(symbol4);
		listOfSymbols.add(symbol5);
		listOfSymbols.add(symbol6);
		
		ArrayList<Symbol> newListOfSymbols = jp.removeWhiteSpace(listOfSymbols);
		assertEquals(Symbol.Type.LEFT_CURLY_BRACKET, newListOfSymbols.get(0).type);
		assertEquals(Symbol.Type.RIGHT_CURLY_BRACKET, newListOfSymbols.get(1).type);
	}
	
	
	//Some examples provide in the task to start with.
	@Test
	public void example1() throws IOException, JSONException {
		jp.parse("{\r\n" + 
				"  \"id\": \"s113867\",\r\n" + 
				"  \"tasks\": [\r\n" + 
				"    \"/task/452359-4435382-6595137\",\r\n" + 
				"    \"/task/99012-65325148-3574826\"\r\n" + 
				"  ]\r\n" + 
				"}\r\n" + 
				"");
		
		assertEquals(2, jp.mainObject.objectContents.size());
		assertEquals("s113867", jp.mainObject.objectContents.get("id"));
		JSONArray jsonArray = (JSONArray) jp.mainObject.objectContents.get("tasks");
		assertEquals(2, jsonArray.arrayContents.size());
		assertEquals("/task/452359-4435382-6595137", jsonArray.arrayContents.get(0));
		assertEquals("/task/99012-65325148-3574826", jsonArray.arrayContents.get(1));
	}
	
	@Test
	public void example2() throws IOException, JSONException {
		jp.parse("{\r\n" + 
				"  \"instruction\": \"add\",\r\n" + 
				"  \"parameters\": [\r\n" + 
				"    \"23\",\r\n" + 
				"    45\r\n" + 
				"  ],\r\n" + 
				"  \"response URL\": \"/answer/d3ae45\"\r\n" + 
				"}\r\n" + 
				"");
		
		assertEquals(3, jp.mainObject.objectContents.size());
		assertEquals("add", jp.mainObject.objectContents.get("instruction"));
		JSONArray jsonArray = (JSONArray) jp.mainObject.objectContents.get("parameters");
		assertEquals(2, jsonArray.arrayContents.size());
		assertEquals("23", jsonArray.arrayContents.get(0));
		assertEquals((long) 45, jsonArray.arrayContents.get(1));
		assertEquals("/answer/d3ae45", jp.mainObject.objectContents.get("response URL"));
	}

}
